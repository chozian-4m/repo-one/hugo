# Hugo

Hugo is a static HTML and CSS website generator written in Go. It is optimized
for speed, ease of use, and configurability. Hugo takes a directory with content
and templates and renders them into a full HTML website.

For more info see the Hugo's [github](https://github.com/gohugoio/hugo).

## Usage

Typical local usage of this image involves mounting a volume of your static site
and performing the server command. To create a quickstart static site, follow
the instructions [here](https://gohugo.io/getting-started/quick-start/).

Once the site is created and you are in the top level of the `quickstart`
directory you serve the site like so:

```
docker run --rm -it -v $PWD:/home/hugo -p 1313:1313 registry1.dsop.io/ironbank/opensource/hugo:0.76.4 server --bind 0.0.0.0
```
