ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8-minimal
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY hugo.tar.gz hugo.tar.gz

RUN microdnf update && \
    microdnf -y install shadow-utils tar gzip --setopt=tsflags=nodocs && \
    groupadd -r hugo && \
    useradd -m -r hugo -g hugo && \
    tar -xzf hugo.tar.gz hugo && \
    install -m 755 hugo /usr/bin && \
    rm -rf hugo.tar.gz && \
    microdnf -y remove shadow-utils tar gzip && \
    microdnf clean all

USER hugo
WORKDIR /home/hugo

EXPOSE 1313

HEALTHCHECK NONE

ENTRYPOINT ["hugo"]
